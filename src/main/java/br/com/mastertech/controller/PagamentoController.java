package br.com.mastertech.controller;

import br.com.mastertech.model.Pagamento;
import br.com.mastertech.model.mapper.PagamentoMapper;
import br.com.mastertech.request.PagamentoRequest;
import br.com.mastertech.response.PagamentoResponse;
import br.com.mastertech.service.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class PagamentoController {

    @Autowired
    private PagamentoService pagamentoService;

    @Autowired
    private PagamentoMapper pagamentoMapper;


    @PostMapping("/pagamento")
    @ResponseBody
    public PagamentoResponse createPagamento(@RequestBody @Valid PagamentoRequest request){
        Pagamento pagamento = pagamentoMapper.toPagamento(request);
        pagamento = pagamentoService.createPagamento(pagamento);
        return pagamentoMapper.toResponse(pagamento);
    }
    @GetMapping("/pagamentos/{idCartao}")
    public List<PagamentoResponse> getPagamentos(@PathVariable(value = "idCartao")long id){
        Pagamento pagamento = pagamentoMapper.toPagamentoByIdCartao(id);
        return pagamentoMapper.toResponseList(pagamentoService.getPagamentos(pagamento));
    }

    @GetMapping("/fatura/{idCliente}/{idCartao}")
    public List<PagamentoResponse> getPagamentoByClient(@PathVariable(value = "idCliente")long idCliente,@PathVariable(value = "idCartao")long id){

        Pagamento pagamento = pagamentoMapper.toPagamentoByIdCliente(idCliente,id);
        return pagamentoMapper.toResponseList(pagamentoService.getPagamentosByCliente(pagamento));
    }

}
