package br.com.mastertech.controller;

import br.com.mastertech.model.Cartao;
import br.com.mastertech.model.mapper.CartaoMapper;
import br.com.mastertech.request.CartaoRequest;
import br.com.mastertech.response.CartaoResponse;
import br.com.mastertech.response.ConsultaCartaoResponse;
import br.com.mastertech.service.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class CartaoController {

    @Autowired
    private CartaoService cartaoService;
    @Autowired
    private CartaoMapper cartaoMapper;

    @PostMapping("/cartao")
    @ResponseStatus(HttpStatus.CREATED)
    public CartaoResponse createCartao(@RequestBody @Valid CartaoRequest dadosCartao, BindingResult result) throws Exception {
            Cartao cartao = cartaoMapper.toCartao(dadosCartao);
            cartao = cartaoService.create(cartao);
            return cartaoMapper.toCartaoResponse(cartao);
    }
    @PatchMapping("/cartao/ativa/{numero}")
    @ResponseStatus(HttpStatus.OK)
    public CartaoResponse ativaCartao(@PathVariable(value = "numero") String numero){
        Cartao cartao = cartaoService.active(numero);
        return cartaoMapper.toCartaoResponse(cartao);
    }

    @GetMapping("/cartao/{numero}")
    @ResponseBody
    public ConsultaCartaoResponse  getCartao(@PathVariable(value = "numero") String numero){
        Cartao cartao = cartaoService.getCartao(numero);
        return cartaoMapper.toConsultaCartaoResponse(cartao);
    }

    @PatchMapping("/fatura/{cliente-id}/{cartao-id}/expirar")
    @ResponseStatus(HttpStatus.OK)
    public CartaoResponse desativaCartao(@PathVariable(value = "cliente-id}") long idCliente,@PathVariable(value = "cartao-id}") long idCartao){

        Cartao cartao = cartaoService.inactive(cartaoMapper.toCartao(idCliente,idCartao));
        return cartaoMapper.toCartaoResponse(cartao);
    }
}
