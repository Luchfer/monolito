package br.com.mastertech.controller;

import br.com.mastertech.model.Cliente;
import br.com.mastertech.model.mapper.ClienteMapper;
import br.com.mastertech.request.ClienteRequest;
import br.com.mastertech.response.ClienteCreatedResponse;
import br.com.mastertech.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;
    @Autowired
    private ClienteMapper clienteMapper;


    @GetMapping("/status")
    @ResponseBody
    public ResponseEntity<String> getStatus() throws Exception {
        return ResponseEntity.status(HttpStatus.OK).body("Service is up");
    }
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ClienteCreatedResponse createCliente(@RequestBody @Valid ClienteRequest clienteRequest, BindingResult result) throws Exception {
        Cliente cliente = clienteMapper.toCliente(clienteRequest);
        cliente=clienteService.createCliente(cliente);
        return clienteMapper.toClienteCreatedResponse(cliente);

    }
    @GetMapping("/{idCliente}")
    @ResponseBody
    public ClienteCreatedResponse getCliente(@PathVariable(value = "idCliente") long idCliente){
        Cliente cliente = clienteService.getCliente(idCliente);
        return clienteMapper.toClienteCreatedResponse(cliente);
    }
}
