package br.com.mastertech.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code= HttpStatus.NOT_FOUND,reason="card id not found or card not active!")
public class IdCartaoNotFoundException extends RuntimeException {
}
