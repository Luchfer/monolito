package br.com.mastertech.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code= HttpStatus.NOT_FOUND,reason="card number not found!")
public class NumeroCartaoNotFoundException extends RuntimeException {
}
