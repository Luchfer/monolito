package br.com.mastertech.request;

import javax.validation.constraints.NotBlank;

public class ClienteRequest {

    @NotBlank
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
