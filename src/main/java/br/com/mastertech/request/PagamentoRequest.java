package br.com.mastertech.request;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

public class PagamentoRequest {

    private long cartao_id;
    @Size(min = 3)
    private String descricao;
    @NotNull
    private BigDecimal valor;
    @NotNull(message = "IdCartao is null")
    public long getCartao_id() {
        return cartao_id;
    }

    public void setCartao_id(long cartao_id) {
        this.cartao_id = cartao_id;
    }

    @NotNull(message = "descricao is null")
    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    @NotNull(message = "valor is null")
    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }
}
