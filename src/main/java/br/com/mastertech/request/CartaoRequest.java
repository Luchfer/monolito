package br.com.mastertech.request;

import javax.validation.constraints.NotNull;

public class CartaoRequest {

    private long clienteId;
    private String numero;

    @NotNull(message = "clientId is null")
    public long getClienteId() {
        return clienteId;
    }

    public void setClienteId(long idCliente) {
        this.clienteId = idCliente;
    }
    @NotNull(message = "numero is null")
    public String getNumero() {
        return numero;
    }



    public void setNumero(String numero) {
        this.numero = numero;
    }


}
