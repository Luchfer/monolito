package br.com.mastertech.repository;

import br.com.mastertech.model.Cartao;
import br.com.mastertech.model.Pagamento;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface  PagamentoRepository extends JpaRepository<Pagamento, Long> {

    List<Pagamento> findByDadosCartao(Cartao dadosCartao);
}
