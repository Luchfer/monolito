package br.com.mastertech.repository;

import br.com.mastertech.model.Cartao;
import br.com.mastertech.model.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CartaoRepository extends JpaRepository<Cartao, Long> {

    Optional<Cartao> findByNumero(String numero);

    Optional<Cartao>  findByDadosClienteAndId(Cliente dadosCliente, long id);

    Optional<Cartao> findByIdAndAtivo(long id, boolean ativo);
}
