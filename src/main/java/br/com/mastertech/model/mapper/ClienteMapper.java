package br.com.mastertech.model.mapper;

import br.com.mastertech.model.Cliente;
import br.com.mastertech.request.ClienteRequest;
import br.com.mastertech.response.ClienteCreatedResponse;
import org.springframework.stereotype.Component;

@Component
public class ClienteMapper {

    public Cliente toCliente(ClienteRequest clienteRequest){
        Cliente cliente = new Cliente();
        cliente.setName(clienteRequest.getName());
        return cliente;
    }

    public ClienteCreatedResponse toClienteCreatedResponse(Cliente cliente) {
        ClienteCreatedResponse clienteCreatedResponse = new ClienteCreatedResponse();
        clienteCreatedResponse.setId(cliente.getId());
        clienteCreatedResponse.setName(cliente.getName());
        return clienteCreatedResponse;
    }


}