package br.com.mastertech.model.mapper;

import br.com.mastertech.model.Cartao;
import br.com.mastertech.model.Cliente;
import br.com.mastertech.model.Pagamento;
import br.com.mastertech.request.PagamentoRequest;
import br.com.mastertech.response.PagamentoResponse;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PagamentoMapper {

    public Pagamento toPagamento(PagamentoRequest pagamentoRequest){
        Pagamento pagamento = new Pagamento();
        pagamento.setDadosCartao(new Cartao(pagamentoRequest.getCartao_id()));
        pagamento.setDescricao(pagamentoRequest.getDescricao());
        pagamento.setValor(pagamentoRequest.getValor());
        return pagamento;
    }

    public PagamentoResponse toResponse(Pagamento pagamento){
        PagamentoResponse response = new PagamentoResponse();
        response.setId(pagamento.getId());
        response.setDescricao(pagamento.getDescricao());
        response.setIdCartao(pagamento.getDadosCartao().getId());
        response.setValor(pagamento.getValor());
        return response;
    }

    public Pagamento toPagamentoByIdCartao(long idCartao){
        Pagamento pagamento = new Pagamento();
        pagamento.setDadosCartao(new Cartao(idCartao));
        return pagamento;
    }

    public Pagamento toPagamentoByIdCliente(long idCiente,long idCartao){
        Pagamento pagamento = new Pagamento();
        Cartao dadosCartao = new Cartao();
        dadosCartao.setId(idCartao);
        dadosCartao.setDadosCliente(new Cliente(idCiente));
        pagamento.setDadosCartao(dadosCartao);
        return pagamento;
    }

    public List<PagamentoResponse> toResponseList(List<Pagamento> pagamentos){
        List<PagamentoResponse> response = new ArrayList<PagamentoResponse>();
        for (Pagamento pagamento:pagamentos) {
            PagamentoResponse responseData = new PagamentoResponse();
            responseData.setId(pagamento.getId());
            responseData.setDescricao(pagamento.getDescricao());
            responseData.setIdCartao(pagamento.getDadosCartao().getId());
            responseData.setValor(pagamento.getValor());
            response.add(responseData);
        }

        return response;
    }
}
