package br.com.mastertech.model.mapper;

import br.com.mastertech.model.Cartao;
import br.com.mastertech.model.Cliente;
import br.com.mastertech.request.CartaoRequest;
import br.com.mastertech.response.CartaoResponse;
import br.com.mastertech.response.ConsultaCartaoResponse;
import org.springframework.stereotype.Component;

@Component
public class CartaoMapper {

    public Cartao toCartao(CartaoRequest request){
        Cartao cartao = new Cartao();

        cartao.setDadosCliente(new Cliente(request.getClienteId()));
        cartao.setNumero(request.getNumero());
        return cartao;
    }

    public Cartao toCartao(long idCliente, long idCartao){
        Cartao cartao = new Cartao();
        cartao.setDadosCliente(new Cliente(idCliente));
        cartao.setId(idCartao);
        return cartao;
    }

    public CartaoResponse toCartaoResponse(Cartao cartao){
        CartaoResponse response = new CartaoResponse();
        response.setId(cartao.getId());
        response.setAtivo(cartao.isAtivo());
        response.setIdCliente(cartao.getDadosCliente().getId());
        response.setNumero(cartao.getNumero());
        return response;
    }

    public ConsultaCartaoResponse toConsultaCartaoResponse(Cartao cartao){
        ConsultaCartaoResponse response = new ConsultaCartaoResponse();
        response.setId(cartao.getId());
        response.setIdCliente(cartao.getDadosCliente().getId());
        response.setNumero(cartao.getNumero());
        return response;
    }

}
