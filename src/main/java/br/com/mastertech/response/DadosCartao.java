package br.com.mastertech.response;

import javax.validation.constraints.NotNull;

public class DadosCartao {

    private long id;
    private long clienteId;
    private String numero;
    private int  ativo;

    public DadosCartao(){}

    public DadosCartao(long id, long clienteId, String numero, int ativo) {
        this.id = id;
        this.clienteId = clienteId;
        this.numero = numero;
        this.ativo = ativo;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
    @NotNull(message = "ClientId is null")
    public long getClienteId() {
        return clienteId;
    }

    public void setClienteId(long clienteId) {
        this.clienteId = clienteId;
    }
    @NotNull(message = "numero is null")
    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public int getAtivo() {
        return ativo;
    }

    public void setAtivo(int ativo) {
        this.ativo = ativo;
    }
}
