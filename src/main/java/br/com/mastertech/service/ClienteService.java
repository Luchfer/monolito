package br.com.mastertech.service;

import br.com.mastertech.exception.ClientNotFoundException;
import br.com.mastertech.model.Cliente;
import br.com.mastertech.model.mapper.ClienteMapper;
import br.com.mastertech.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;



@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;
    @Autowired
    private ClienteMapper clienteMapper;

    public Cliente createCliente(Cliente dadosCliente){
        dadosCliente = clienteRepository.save(dadosCliente);
        return dadosCliente;
    }


    public Cliente getCliente(Long idCliente){
        Optional<Cliente> opCliente = clienteRepository.findById(idCliente);
        if(!opCliente.isPresent()){
            throw new ClientNotFoundException();
        }
        return opCliente.get();
    }


}
