package br.com.mastertech.service;

import br.com.mastertech.exception.NumeroCartaoNotFoundException;
import br.com.mastertech.model.Cartao;
import br.com.mastertech.repository.CartaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    private CartaoRepository  cartaoRepository;
    @Autowired
    private ClienteService clienteService;

    public Cartao create(Cartao dadosCartao){
        dadosCartao.setAtivo(false);
        clienteService.getCliente(dadosCartao.getDadosCliente().getId());
        return cartaoRepository.save(dadosCartao);
    }

    public Cartao active(String numero){
        Optional<Cartao> opCartao = cartaoRepository.findByNumero(numero);
        if(!opCartao.isPresent()){
            throw new NumeroCartaoNotFoundException();
        }
        Cartao dadosCartao = opCartao.get();
        dadosCartao.setAtivo(true);
        return cartaoRepository.save(dadosCartao);
    }

    public Cartao inactive(Cartao cartao){
        Optional<Cartao> opCartao = cartaoRepository.findByDadosClienteAndId(cartao.getDadosCliente(),cartao.getId());
        if(!opCartao.isPresent()){
            throw new NumeroCartaoNotFoundException();
        }
        Cartao dadosCartao = opCartao.get();
        dadosCartao.setAtivo(false);
        return cartaoRepository.save(dadosCartao);
    }

    public Cartao getCartao(String numero){
        Optional<Cartao> opCartao = cartaoRepository.findByNumero(numero);
        if(!opCartao.isPresent()){
            throw new NumeroCartaoNotFoundException();
        }
        Cartao dadosCartao = opCartao.get();
        return cartaoRepository.save(dadosCartao);
    }

}
