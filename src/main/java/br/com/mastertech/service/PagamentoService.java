package br.com.mastertech.service;

import br.com.mastertech.exception.IdCartaoNotFoundException;
import br.com.mastertech.model.Cartao;
import br.com.mastertech.model.Pagamento;
import br.com.mastertech.repository.CartaoRepository;
import br.com.mastertech.repository.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class PagamentoService {
    @Autowired
    private PagamentoRepository pagamentoRepository;
    @Autowired
    private CartaoRepository cartaoRepository;

    public Pagamento createPagamento(Pagamento dadosPagamento){
        Optional<Cartao> opCartao = cartaoRepository.findByIdAndAtivo(dadosPagamento.getDadosCartao().getId(),true);
        if(!opCartao.isPresent()){
            throw new IdCartaoNotFoundException();
        }
        dadosPagamento.setDataCompra(LocalDateTime.now());
        return pagamentoRepository.save(dadosPagamento);
    }

    public List<Pagamento> getPagamentos(Pagamento dadosPagamento){
        List<Pagamento>  pagamentos = pagamentoRepository.findByDadosCartao(dadosPagamento.getDadosCartao());
        return pagamentos;
    }

    public List<Pagamento> getPagamentosByCliente(Pagamento dadosPagamento){
        Optional<Cartao> opCartao = cartaoRepository.findByDadosClienteAndId(dadosPagamento.getDadosCartao().getDadosCliente(),dadosPagamento.getDadosCartao().getId());
        if(!opCartao.isPresent()){
            throw new IdCartaoNotFoundException();
        }
        List<Pagamento>  pagamentos = pagamentoRepository.findByDadosCartao(dadosPagamento.getDadosCartao());
        return pagamentos;
    }
}
